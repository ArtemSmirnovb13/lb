<?php $on = isset($on) ? $on : null; ?>
<?php $release = isset($release) ? $release : null; ?>
<?php $date = isset($date) ? $date : null; ?>
<?php $chmods = isset($chmods) ? $chmods : null; ?>
<?php $branch = isset($branch) ? $branch : null; ?>
<?php $repo = isset($repo) ? $repo : null; ?>
<?php $current = isset($current) ? $current : null; ?>
<?php $path = isset($path) ? $path : null; ?>
<?php $timezone = isset($timezone) ? $timezone : null; ?>
<?php $user = isset($user) ? $user : null; ?>
<?php
  $user = 'root';
  $timezone = 'Europe/Moscow';

  $path = '/var/www/depl';
  $current = $path . '/current';

  $repo = "https://ArtemSmirnovb13@bitbucket.org/ArtemSmirnovb13/lb.git";
  $branch = 'master';

  $chmods = [
    'storage/logs'
  ];

  $date = new datetime('now', new DateTimeZone($timezone));
  $release = $path . '/releases/. $date->format('YmdHis');
?>

<?php $__container->servers(['production' => $user . '@45.128.204.190']); ?>

<?php $__container->startTask('clone', ['on' => $on]); ?>
  mkdir -p <?php echo $release; ?>


  git clone --depth 1 -b <?php echo $branch; ?> "<?php echo $repo; ?>" <?php echo $release; ?>


  echo "#1 - Repository has been cloned"
<?php $__container->endTask(); ?>

<?php $__container->startTask('composer', ['on' => $on]); ?>
  composer self-update

  cd <?php echo $release; ?>


  composer install --no-interaction --no-dev --prefer-dist

  echo "#2 - Composer dependecies have been installed"
<?php $__container->endTask(); ?>

<?php $__container->startTask('artisan', ['on' => $on]); ?>
  cd <?php echo $release; ?>


  ln -nfs <?php echo $path; ?>/.env .env;
  chgrp -h www-data .env;

  php artisan config:clear

  echo "#3 - Composer dependecies have been installed"
<?php $__container->endTask(); ?>

<?php $__container->startTask('chmod', ['on' => $on]); ?>

  chmod 777 -R /var/www/depl

  echo "#4 - Permissions has been set"
<?php $__container->endTask(); ?>

<?php $__container->startTask('update_symlinks', ['on' => $on]); ?>
  ln -nfs <?php echo $release; ?> <?php echo $current; ?>;
  chgrp -h www-data <?php echo $current; ?>;

  echo "#5 - Symlink has been set"
<?php $__container->endTask(); ?>

<?php $__container->startMacro('deploy', ['on' => 'production']); ?>
  clone
  composer
  artisan
  chmod
  update_symlinks
<?php $__container->endMacro(); ?>
