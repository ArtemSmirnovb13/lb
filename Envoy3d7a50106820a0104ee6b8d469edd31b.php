<?php $symlink = isset($symlink) ? $symlink : null; ?>
<?php $folder = isset($folder) ? $folder : null; ?>
<?php $file = isset($file) ? $file : null; ?>
<?php $on = isset($on) ? $on : null; ?>
<?php $release = isset($release) ? $release : null; ?>
<?php $date = isset($date) ? $date : null; ?>
<?php $symlinks = isset($symlinks) ? $symlinks : null; ?>
<?php $chmods = isset($chmods) ? $chmods : null; ?>
<?php $hasHtmlPurifier = isset($hasHtmlPurifier) ? $hasHtmlPurifier : null; ?>
<?php $keep = isset($keep) ? $keep : null; ?>
<?php $branch = isset($branch) ? $branch : null; ?>
<?php $repo = isset($repo) ? $repo : null; ?>
<?php $current = isset($current) ? $current : null; ?>
<?php $path = isset($path) ? $path : null; ?>
<?php $timezone = isset($timezone) ? $timezone : null; ?>
<?php $user = isset($user) ? $user : null; ?>
<?php
123
$user = 'root';
<?php /* The timezone your servers run in */ ?>
$timezone = 'Europe/Moscow';

<?php /* The base path where your deployments are sitting */ ?>
$path = '/var/www/test';

$current = $path . '/current';

<?php /* The git repository location */ ?>
$repo = 'clone https://ArtemSmirnovb13@bitbucket.org/ArtemSmirnovb13/lb.git';

<?php /* The git branch to deploy */ ?>
$branch = 'master';

<?php /* The number of releases to keep */ ?>
$keep = 6;

<?php /* Is the HTMLPurifier library installed */ ?>
$hasHtmlPurifier = true;

<?php /* Files and direcrtories that need permissions of 755 and www-data as owner */ ?>
$chmods = [
'app/storage',
'app/database/production.sqlite',
'public',
];

<?php /* All directories symlinked to the shared folder */ ?>
$symlinks = [
'storage/views'    => 'app/storage/views',
'storage/sessions' => 'app/storage/sessions',
'storage/logs'     => 'app/storage/logs',
'storage/cache'    => 'app/storage/cache',
];

<?php /* DO NOT EDIT BELOW THIS LINE */ ?>
$date    = new DateTime('now', new DateTimeZone($timezone));
$release = $path .'/releases/'. $date->format('YmdHis');
?>

<?php /* Clone task, creates release directory, then shallow clones into it */ ?>
<?php $__container->startTask('clone', ['on' => $on]); ?>
mkdir -p <?php echo $release; ?>


git clone --depth 1 -b <?php echo $branch; ?> "<?php echo $repo; ?>" <?php echo $release; ?>


echo "Repository has been cloned"
<?php $__container->endTask(); ?>

<?php /* Updates composer, then runs a fresh installation */ ?>
<?php $__container->startTask('composer', ['on' => $on]); ?>
composer self-update

cd <?php echo $release; ?>


composer install --no-interaction --no-dev --prefer-dist

echo "Composer dependencies have been installed"
<?php $__container->endTask(); ?>

<?php /* Set permissions for various files and directories */ ?>
<?php $__container->startTask('chmod', ['on' => $on]); ?>
<?php foreach($chmods as $file): ?>
    chmod -R 755 <?php echo $release; ?>/<?php echo $file; ?>


    chmod -R g+s <?php echo $release; ?>/<?php echo $file; ?>


    chown -R www-data:www-data <?php echo $release; ?>/<?php echo $file; ?>


    echo "Permissions have been set for <?php echo $file; ?>"
<?php endforeach; ?>

<?php if($hasHtmlPurifier): ?>
    chmod -R 777 <?php echo $release; ?>/vendor/ezyang/htmlpurifier/library/HTMLPurifier/DefinitionCache/Serializer
<?php endif; ?>

echo "Permissions for HTMLPurifier have been set"
<?php $__container->endTask(); ?>

<?php /* Symlink some folders */ ?>
<?php $__container->startTask('symlinks', ['on' => $on]); ?>
<?php foreach($symlinks as $folder => $symlink): ?>
    ln -s <?php echo $path; ?>/shared/<?php echo $folder; ?> <?php echo $release; ?>/<?php echo $symlink; ?>


    echo "Symlink has been set for <?php echo $symlink; ?>"
<?php endforeach; ?>

echo "All symlinks have been set"
<?php $__container->endTask(); ?>

<?php /* Set the symlink for the current release */ ?>
<?php $__container->startTask('update-symlink', ['on' => $on]); ?>
rm -rf <?php echo $path; ?>/current

ln -s <?php echo $release; ?> <?php echo $path; ?>/current

echo "Release symlink has been set"
<?php $__container->endTask(); ?>

<?php /* Migrate all databases */ ?>
<?php $__container->startTask('migrate', ['on' => $on]); ?>
php <?php echo $release; ?>/artisan migrate
<?php $__container->endTask(); ?>

<?php /* Just a done message :) */ ?>
<?php $__container->startTask('done', ['on' => $on]); ?>
echo "Deployment finished successfully!"
<?php $__container->endTask(); ?>

<?php /* Run all deployment tasks */ ?>
<?php $__container->startMacro('deploy'); ?>
clone
composer
chmod
migrate
symlinks
update-symlink
done
<?php $__container->endMacro(); ?>
